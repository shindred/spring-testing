package com.learn.testing.controller;

import com.learn.testing.entity.User;
import com.learn.testing.service.UserAccountService;
import com.learn.testing.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private User userMock;

    @Mock
    private Page<User> userPageMock;

    private final long id = 1L;
    private final String name = "n";
    private final String email = "e";
    private final int pageNum = 1;
    private final int pageSize = 1;

    @Before
    public void setUp() {
        when(userService.getUserById(id)).thenReturn(userMock);
    }

    @Test
    public void getUserByIdTest() {
        assertThat(userController.getUserById(id)).isNotNull();
        verify(userService).getUserById(id);
    }

    @Test
    public void getUserByEmailTest() {
        when(userService.getUserByEmail(email)).thenReturn(userMock);

        assertThat(userController.getUserByEmail(email)).isNotNull();
        verify(userService).getUserByEmail(email);
    }

    @Test
    public void getUsersByNamePerPageTest() {
        when(userService.getUsersByName(name, pageNum, pageSize)).thenReturn(userPageMock);

        assertThat(userController.getUsersByNamePerPage(name, pageNum, pageSize)).isNotNull();
        verify(userService).getUsersByName(name, pageNum, pageSize);
    }

    @Test
    public void deleteUserByIdTest() {
        userController.deleteUserById(id);

        verify(userService).deleteUser(id);
    }

    @Test
    public void updateUserByIdTest() {
        when(userService.updateUser(userMock)).thenReturn(userMock);

        assertThat(userController.updateUserById(id, userMock)).isNotNull();
        verify(userService).updateUser(userMock);
    }

    @Test
    public void createUserTest() {
        User user = new User(name, email);
        when(userService.createUser(user)).thenReturn(userMock);

        assertThat(userController.createUser(name, email)).isNotNull();
        verify(userService).createUser(user);
    }

    @Test
    public void refill() {
        int amount = 1;

        userController.refill(id, amount);

        verify(userService).getUserById(id);
        verify(userAccountService).refill(userMock, amount);
    }
}