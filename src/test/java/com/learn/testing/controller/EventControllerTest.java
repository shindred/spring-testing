package com.learn.testing.controller;

import com.learn.testing.controller.dateparser.DateParser;
import com.learn.testing.entity.Event;
import com.learn.testing.service.EventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventControllerTest {

    @InjectMocks
    private EventController eventController;

    @Mock
    private EventService eventServiceMock;

    @Mock
    private DateParser dateParserMock;

    @Mock
    private Event eventMock;

    @Mock
    private Page<Event> eventPageMock;

    @Mock
    private Date dateMock;

    private final long id = 1L;
    private final int pageNum = 1;
    private final int pageSize = 1;

    @Test
    public void getEventByIdTest() {
        when(eventServiceMock.getEventById(id)).thenReturn(eventMock);

        assertThat(eventController.getEventById(id)).isNotNull();
        verify(eventServiceMock).getEventById(id);
    }

    @Test
    public void getEventByTitleTest() {
        String title = "t";
        when(eventServiceMock.getEventByTitle(title, pageNum, pageSize)).thenReturn(eventPageMock);

        assertThat(eventController.getEventByTitle(title, pageNum, pageSize)).isNotNull();
        verify(eventServiceMock).getEventByTitle(title, pageNum, pageSize);
    }

    @Test
    public void createEventTest() {
        when(eventServiceMock.createEvent(eventMock)).thenReturn(eventMock);

        assertThat(eventController.createEvent(eventMock)).isNotNull();
        verify(eventServiceMock).createEvent(eventMock);
    }

    @Test
    public void getEventsForDayTest() {
        String dateString = "2021-02-02";
        when(dateParserMock.parseDate(dateString)).thenReturn(dateMock);
        when(eventServiceMock.getEventsForDay(dateMock, pageNum, pageSize)).thenReturn(eventPageMock);

        assertThat(eventController.getEventsForDay(dateString, pageNum, pageSize)).isNotNull();
        verify(dateParserMock).parseDate(dateString);
        verify(eventServiceMock).getEventsForDay(dateMock, pageNum, pageSize);
    }

    @Test
    public void updateEventTest() {
        when(eventServiceMock.updateEvent(eventMock)).thenReturn(eventMock);

        assertThat(eventController.updateEvent(eventMock)).isNotNull();
        verify(eventServiceMock).updateEvent(eventMock);
    }

    @Test
    public void deleteEventByIdTest() {
        eventController.deleteEventById(id);

        verify(eventServiceMock).deleteEvent(id);
    }
}