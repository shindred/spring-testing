package com.learn.testing.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.entity.enumaration.TicketCategory;
import com.learn.testing.repository.EventRepository;
import com.learn.testing.repository.TicketRepository;
import com.learn.testing.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TicketControllerTest {

    @Autowired
    private TicketController ticketController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TicketRepository ticketRepository;

    private MockMvc mockMvc;
    private Ticket ticket;
    private final long userId = 1L;
    private final long eventId = 1L;
    private final TicketCategory category = TicketCategory.STANDARD;
    private final int pageNum = 1;
    private final int pageSize = 1;
    private final int place = 1;
    private final String title = "t";
    private final int price = 1;
    private final String userName = "n";
    private final String userEmail = "e";

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(ticketController).build();
        ticket = new Ticket(new User(userId), new Event(eventId), place, category);
        userRepository.save(new User(userId, userName, userEmail));
        eventRepository.save(new Event(eventId, title, Calendar.getInstance().getTime(), price));
        ticketRepository.save(ticket);
    }

    @Test
    public void bookTicketTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                post("/ticket")
                        .param("userId", String.valueOf(userId))
                        .param("eventId", String.valueOf(eventId))
                        .param("place", String.valueOf(place))
                        .param("ticketCategoryName", category.toString())
        )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mvcResult.getAsyncResult()).usingRecursiveComparison().ignoringActualNullFields().isEqualTo(ticket);
    }

    @Test
    public void getBookedTicketsByUserTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get("/ticket/by/user/{userId}", userId)
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .contains(
                        "\"title\":\"" + title + "\"",
                        "\"price\":" + price,
                        "\"name\":\"" + userName + "\"",
                        "\"email\":\"" + userEmail + "\"",
                        "\"ticketCategory\":\"" + category + "\"",
                        "\"place\":" + place
                );
    }

    @Test
    public void getBookedTicketsByEventTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get("/ticket/by/event/{eventId}", eventId)
                        .param("pageNum", String.valueOf(pageNum))
                        .param("pageSize", String.valueOf(pageSize))
        )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .contains(
                        "\"title\":\"" + title + "\"",
                        "\"price\":" + price,
                        "\"name\":\"" + userName + "\"",
                        "\"email\":\"" + userEmail + "\"",
                        "\"ticketCategory\":\"" + category + "\"",
                        "\"place\":" + place
                );
    }

    @Test
    public void cancelTicketTest() throws Exception {
        long ticketId = 1L;

        mockMvc.perform(
                delete("/ticket/{id}", ticketId)
        )
                .andExpect(status().isOk());

        assertThat(ticketRepository.findById(ticketId)).isEmpty();
    }
}