package com.learn.testing.exception.thrower;

import com.learn.testing.exception.IdNotNullException;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class IdNotNullExceptionThrowerTest {

    @Test
    public void throwIfIdNotNullTest() {
        assertThrows(
                IdNotNullException.class,
                () -> new IdNotNullExceptionThrower().throwIfIdNotNull(1L)
        );
    }
}