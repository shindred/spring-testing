package com.learn.testing.exception.thrower;

import com.learn.testing.exception.PaginationParameterException;
import org.junit.Test;

public class PaginationParameterExceptionThrowerTest {

    @Test(expected = PaginationParameterException.class)
    public void throwIfParametersAreLessThanOneTest() {
        PaginationParameterExceptionThrower thrower = new PaginationParameterExceptionThrower();
        thrower.throwIfParametersAreLessThanOne(-1, 1);
    }
}