package com.learn.testing.service.impl;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.exception.thrower.PaginationParameterExceptionThrower;
import com.learn.testing.repository.TicketRepository;
import com.learn.testing.service.EventService;
import com.learn.testing.service.UserAccountService;
import com.learn.testing.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

    @InjectMocks
    private TicketServiceImpl ticketService;

    @Mock
    private TicketRepository ticketRepositoryMock;

    @Mock
    private UserService userServiceMock;

    @Mock
    private EventService eventServiceMock;

    @Mock
    private UserAccountService userAccountServiceMock;

    @Mock
    private Ticket ticketMock;

    @Mock
    private User userMock;

    @Mock
    private Event eventMock;

    @Mock
    private PaginationParameterExceptionThrower thrower;

    private final long id = 1L;

    @Test
    public void bookTicketTest() {
        when(ticketMock.getEvent()).thenReturn(eventMock);
        when(ticketMock.getUser()).thenReturn(userMock);
        when(userMock.getId()).thenReturn(id);
        when(eventMock.getId()).thenReturn(id);
        when(userServiceMock.getUserById(id)).thenReturn(userMock);
        when(eventServiceMock.getEventById(id)).thenReturn(eventMock);
        when(eventMock.getPrice()).thenReturn(1);

        ticketService.bookTicket(ticketMock);

        verify(eventServiceMock, times(1)).getEventById(id);
        verify(userAccountServiceMock, times(1)).withdraw(userMock, 1);
        verify(ticketRepositoryMock, times(1)).save(any(Ticket.class));
    }

    @Test
    public void getBookedTicketsByUserTest() {
        ticketService.getBookedTicketsByUser(userMock, 1, 1);
        verify(ticketRepositoryMock, times(1)).findBookedTicketsByUser(userMock, PageRequest.of(0, 1));
    }

    @Test
    public void getBookedTicketsByEventWithValidInputTest() {
        ticketService.getBookedTicketsByEvent(eventMock, 1, 1);
        verify(ticketRepositoryMock, times(1)).findBookedTicketsByEvent(eventMock, PageRequest.of(0, 1));
    }

    @Test
    public void cancelTicketTest() {
        ticketService.cancelTicket(id);
        verify(ticketRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    public void getAllTicketsTest() {
        ticketService.getAllTickets();
        verify(ticketRepositoryMock, times(1)).findAll();
    }

    @Test
    public void bookTicketsTest() {
        when(ticketMock.getUser()).thenReturn(userMock);
        when(ticketMock.getEvent()).thenReturn(eventMock);
        List<Ticket> ticketList = Collections.singletonList(ticketMock);
        ticketService.bookTickets(ticketList);
        verify(ticketRepositoryMock, times(1)).saveAll(ticketList);
    }
}