package com.learn.testing.jms.sender;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.entity.enumaration.TicketCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class JmsTicketSenderTest {

    @InjectMocks
    private JmsTicketSender jmsTicketSender;

    @Mock
    private JmsTemplate jmsTemplateMock;

    @Test
    public void sendTicketForBookingTest() {
        final String destination = "ticket";
        final long id = 1L;
        final int place = 1;
        final TicketCategory ticketCategory = TicketCategory.STANDARD;
        final Ticket expectedTicket = new Ticket(new User(id), new Event(id), place, ticketCategory);


        Ticket actualTicket = jmsTicketSender.sendTicketForBooking(id, id, place, ticketCategory);

        assertThat(actualTicket).isEqualTo(expectedTicket);
        verify(jmsTemplateMock).convertAndSend(destination, expectedTicket);
    }
}