package com.learn.testing.jms.listener;

import com.learn.testing.entity.Ticket;
import com.learn.testing.service.TicketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JmsTicketListenerTest {

    @InjectMocks
    private JmsTicketListener jmsTicketListener;

    @Mock
    private TicketService ticketServiceMock;

    @Mock
    private Ticket ticketMock;

    @Test
    public void bookTicketTest() {
        when(ticketServiceMock.bookTicket(ticketMock)).thenReturn(ticketMock);

        jmsTicketListener.bookTicket(ticketMock);

        verify(ticketServiceMock).bookTicket(ticketMock);
    }
}