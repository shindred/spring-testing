package com.learn.testing.exception;

public class NotEnoughMoneyException extends IllegalArgumentException {
    public NotEnoughMoneyException() {
        super("Not enough money");
    }
}
