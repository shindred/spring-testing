package com.learn.testing.exception;

import java.util.NoSuchElementException;

public class UserAccountNotFoundException extends NoSuchElementException {
    public UserAccountNotFoundException() {
        super("User account not found");
    }
}
