package com.learn.testing.exception;

import java.util.NoSuchElementException;

public class EventNotFoundException extends NoSuchElementException {
    public EventNotFoundException() {
        super("Event not found");
    }
}
