package com.learn.testing.service;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.exception.PaginationParameterException;
import com.learn.testing.exception.TicketNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TicketService {

    /**
     * Book ticket for a specified event on behalf of specified user.
     *
     * @param ticket Ticket to save
     * @return Created ticket object.
     */
    Ticket bookTicket(Ticket ticket);

    /**
     * Get all booked tickets for specified user.
     *
     * @param user     User
     * @param pageSize Number of tickets to return on a page.
     * @param pageNum  Number of the page to return.
     * @return List of Ticket objects
     * @throws TicketNotFoundException      if tickets were not found..
     * @throws PaginationParameterException if pageSize or pageNum is less than zero
     */
    Page<Ticket> getBookedTicketsByUser(User user, int pageNum, int pageSize);

    /**
     * Get all booked tickets for specified event.
     *
     * @param event    Event
     * @param pageSize Number of tickets to return on a page.
     * @param pageNum  Number of the page to return.
     * @return List of Ticket objects.
     * @throws TicketNotFoundException      if tickets were not found..
     * @throws PaginationParameterException if pageSize or pageNum is less than zero
     */
    Page<Ticket> getBookedTicketsByEvent(Event event, int pageNum, int pageSize);

    /**
     * Cancel ticket by it's id
     *
     * @param ticketId Ticket id
     */
    void cancelTicket(long ticketId);

    /**
     * Get list of all existed tickets
     *
     * @return List of tickets or empty list if nothing was found.
     */
    List<Ticket> getAllTickets();

    /**
     * Book tickets for a specified events on behalf of specified users.
     *
     * @param tickets The List of tickets
     * @return List of created tickets.
     */
    List<Ticket> bookTickets(List<Ticket> tickets);
}
