package com.learn.testing.service;

import com.learn.testing.entity.User;
import com.learn.testing.entity.UserAccount;

public interface UserAccountService {

    UserAccount findByUser(User user);

    UserAccount create(UserAccount userAccount);

    void refill(User user, int amount);

    void withdraw(User user, int amount);
}
