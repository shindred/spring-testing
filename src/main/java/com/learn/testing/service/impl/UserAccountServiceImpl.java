package com.learn.testing.service.impl;

import com.learn.testing.entity.User;
import com.learn.testing.entity.UserAccount;
import com.learn.testing.exception.NotEnoughMoneyException;
import com.learn.testing.exception.UserAccountNotFoundException;
import com.learn.testing.exception.thrower.IdNotNullExceptionThrower;
import com.learn.testing.repository.UserAccountRepository;
import com.learn.testing.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserAccountServiceImpl implements UserAccountService {

    private final UserAccountRepository userAccountRepository;

    private final IdNotNullExceptionThrower idNotNullExceptionThrower;

    @Override
    public UserAccount findByUser(User user) {
        log.debug("Getting user account: " + user.getEmail());
        return userAccountRepository.findByUser(user)
                .orElseThrow(UserAccountNotFoundException::new);
    }

    @Override
    public UserAccount create(UserAccount userAccount) {
        idNotNullExceptionThrower.throwIfIdNotNull(userAccount.getId());
        log.debug("Creating user account for user: " + userAccount.getUser().getEmail());
        return userAccountRepository.save(userAccount);
    }

    @Override
    public void refill(User user, int amount) {
        userAccountRepository.findByUser(user)
                .ifPresent(uAcc -> {
                    int moneyAmount = uAcc.getMoney();
                    uAcc.setMoney(moneyAmount + amount);
                    log.debug("Refilled " + moneyAmount + "  for user: " + user.getEmail());
                    userAccountRepository.save(uAcc);
                });
    }

    @Override
    public void withdraw(User user, int amount) {
        userAccountRepository.findByUser(user)
                .ifPresent(uAcc -> {
                    int accountMoney = uAcc.getMoney();
                    if (accountMoney < amount){
                        throw new NotEnoughMoneyException();
                    }
                    uAcc.setMoney(accountMoney - amount);
                    log.debug("Withdrawn " + accountMoney + " from user :"  + user.getEmail());
                    userAccountRepository.save(uAcc);
                });
    }
}
