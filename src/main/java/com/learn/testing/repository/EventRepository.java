package com.learn.testing.repository;

import com.learn.testing.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface EventRepository extends JpaRepository<Event, Long> {

    Page<Event> findEventsByTitle(String title, Pageable pageable);

    Page<Event> findEventsByDate(Date date, Pageable pageable);
}
