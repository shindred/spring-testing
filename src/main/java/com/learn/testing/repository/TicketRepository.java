package com.learn.testing.repository;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Page<Ticket> findBookedTicketsByEvent(Event event, Pageable pageable);

    Page<Ticket> findBookedTicketsByUser(User user, Pageable pageable);
}
