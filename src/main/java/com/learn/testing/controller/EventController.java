package com.learn.testing.controller;

import com.learn.testing.controller.dateparser.DateParser;
import com.learn.testing.entity.Event;
import com.learn.testing.service.EventService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
@Slf4j
public class EventController {

    private final EventService eventService;
    private final DateParser dateParser;

    @GetMapping("/{id}")
    public Event getEventById(@PathVariable long id) {
        log.info("Loading event page, id: " + id);
        return eventService.getEventById(id);
    }

    @GetMapping("/by/title/{title}")
    public Page<Event> getEventByTitle(@PathVariable String title, @RequestParam int pageNum, @RequestParam int pageSize) {
        log.info("Loading events from DB, title: " + title);
        Page<Event> events = eventService.getEventByTitle(title, pageNum, pageSize);
        log.info("Events loaded");
        return events;
    }

    @PostMapping
    public Event createEvent(Event event) {
        log.info("Saving event");
        Event savedEvent = eventService.createEvent(event);
        Long savedEventId = savedEvent.getId();
        log.info("Event saved, id: " + savedEventId);
        return savedEvent;
    }

    @GetMapping("/by/date/{date}")
    public Page<Event> getEventsForDay(@PathVariable String date, @RequestParam int pageNum, @RequestParam int pageSize) {
        Date parsedDate = dateParser.parseDate(date);
        log.info("Loading events from DB, date: " + parsedDate);
        Page<Event> events = eventService.getEventsForDay(parsedDate, pageNum, pageSize);
        log.info("Events loaded");
        return events;
    }

    @PutMapping("/{id}")
    public Event updateEvent(Event event) {
        return eventService.updateEvent(event);
    }

    @DeleteMapping("/{id}")
    public void deleteEventById(@PathVariable long id) {
        eventService.deleteEvent(id);
    }
}
