package com.learn.testing.controller.dateparser;

import java.util.Date;

public interface DateParser {
    Date parseDate(String dateToParse);
}
