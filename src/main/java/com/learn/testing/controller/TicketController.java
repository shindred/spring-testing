package com.learn.testing.controller;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.entity.enumaration.TicketCategory;
import com.learn.testing.jms.sender.JmsTicketSender;
import com.learn.testing.service.EventService;
import com.learn.testing.service.TicketService;
import com.learn.testing.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ticket")
@Slf4j
public class TicketController {

    private final TicketService ticketService;
    private final UserService userService;
    private final EventService eventService;
    private final JmsTicketSender jmsTicketSender;

    @Async
    @PostMapping
    public CompletableFuture<Ticket> bookTicket(@RequestParam long userId,
                                                @RequestParam long eventId,
                                                @RequestParam int place,
                                                @RequestParam String ticketCategoryName) throws InterruptedException {
        log.info(Thread.currentThread().getName() + " started");
        Thread.sleep(5000);
        log.info("Saving ticket to DB");
        TicketCategory ticketCategory = TicketCategory.valueOf(ticketCategoryName.toUpperCase());
        Ticket sentTicket = jmsTicketSender.sendTicketForBooking(userId, eventId, place, ticketCategory);
        log.info(Thread.currentThread().getName() + " ended");
        return CompletableFuture.completedFuture(sentTicket);
    }

    @GetMapping("/by/user/{userId}")
    public Page<Ticket> getBookedTicketsByUser(@PathVariable long userId,
                                               @RequestParam int pageNum,
                                               @RequestParam int pageSize) {
        log.info("Loading tickets from DB, user id: " + userId);
        User user = userService.getUserById(userId);
        log.info("Tickets loaded");
        return ticketService.getBookedTicketsByUser(user, pageNum, pageSize);
    }

    @GetMapping("/by/event/{eventId}")
    public Page<Ticket> getBookedTicketsByEvent(@PathVariable long eventId,
                                                @RequestParam int pageSize,
                                                @RequestParam int pageNum) {
        log.info("Loading tickets from DB, event id: " + eventId);
        Event event = eventService.getEventById(eventId);
        log.info("Tickets loaded");
        return ticketService.getBookedTicketsByEvent(event, pageNum, pageSize);
    }

    @DeleteMapping("/{id}")
    public void cancelTicket(@PathVariable long id) {
        ticketService.cancelTicket(id);
    }
}
