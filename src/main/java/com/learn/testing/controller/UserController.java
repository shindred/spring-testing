package com.learn.testing.controller;

import com.learn.testing.entity.User;
import com.learn.testing.service.UserAccountService;
import com.learn.testing.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Slf4j
public class UserController {

    private final UserService userService;
    private final UserAccountService userAccountService;

    @GetMapping("/{id}")
    public User getUserById(@PathVariable long id) {
        log.info("Loading user from DB");
        User user = userService.getUserById(id);
        log.info("User loaded");
        return user;
    }

    @GetMapping("/by/email/{email}")
    public User getUserByEmail(@PathVariable String email) {
        log.info("Loading user from DB");
        User foundUser = userService.getUserByEmail(email);
        log.info("User loaded");
        return foundUser;
    }

    @GetMapping("/by/name/{name}")
    public Page<User> getUsersByNamePerPage(@PathVariable String name,
                                            @RequestParam int pageNum,
                                            @RequestParam int pageSize) {
        log.info("Loading users from DB, name: " + name);
        Page<User> userPage = userService.getUsersByName(name, pageNum, pageSize);
        log.info("Users loaded");
        return userPage;
    }

    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{id}")
    public User updateUserById(@PathVariable Long id, User user) {
        user.setId(id);
        return userService.updateUser(user);
    }

    @PostMapping
    public User createUser(@RequestParam String name, @RequestParam String email) {
        log.info("Saving user to a DB");
        User savedUser = userService.createUser(new User(name, email));
        log.info("User saved");
        return savedUser;
    }

    @PutMapping("/user-account/{userId}")
    public void refill(@PathVariable long userId, @RequestParam int amount) {
        log.info("Refilling user account for " + amount);
        userAccountService.refill(userService.getUserById(userId), amount);
        log.info("User account refilled");
    }
}
