package com.learn.testing.entity.enumaration;

public enum TicketCategory {
    STANDARD, PREMIUM, BAR
}
