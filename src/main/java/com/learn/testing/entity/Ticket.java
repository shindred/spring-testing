package com.learn.testing.entity;

import com.learn.testing.entity.enumaration.TicketCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Ticket implements Serializable {

    private static final long serialVersionUID = 1L;

    public Ticket(User user, Event event, int place, TicketCategory ticketCategory) {
        this.event = event;
        this.user = user;
        this.ticketCategory = ticketCategory;
        this.place = place;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    @Getter
    private Long id;

    @ManyToOne
    @JoinColumn(name = "event_id")
    @Setter
    @Getter
    private Event event;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Setter
    @Getter
    private User user;

    @Setter
    @Getter
    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private TicketCategory ticketCategory;

    @Setter
    @Getter
    private int place;
}
