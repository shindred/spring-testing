package com.learn.testing.jms.listener;

import com.learn.testing.entity.Ticket;
import com.learn.testing.service.TicketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class JmsTicketListener {

    private final TicketService ticketService;

    @JmsListener(destination = "ticket", containerFactory = "listenerContainerFactory")
    public void bookTicket(Ticket ticket) {
        log.info("Received ticket info, started booking");

        Ticket savedTicket = ticketService.bookTicket(ticket);

        log.info("Booked ticket with id: " + savedTicket.getId());
    }
}
