package com.learn.testing.jms.sender;

import com.learn.testing.entity.Event;
import com.learn.testing.entity.Ticket;
import com.learn.testing.entity.User;
import com.learn.testing.entity.enumaration.TicketCategory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class JmsTicketSender {

    private static final String DESTINATION = "ticket";

    private final JmsTemplate jmsTemplate;

    public Ticket sendTicketForBooking(long userId, long eventId, int place, TicketCategory ticketCategory) {
        log.info("Sending ticket for booking");

        Ticket ticket = new Ticket(new User(userId), new Event(eventId), place, ticketCategory);

        jmsTemplate.convertAndSend(DESTINATION, ticket);

        log.info("Ticket sent");

        return ticket;
    }
}
